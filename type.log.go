package main

import (
	"database/sql"
	_ "github.com/lib/pq"
)

type Log struct {
	Id                 int     `json:"id"`
	BaseCurrencyCode   string  `json:"base_currency_code"`
	TargetCurrencyCode string  `json:"target_currency_code"`
	CreationDate       int64   `json:"creation_date"`
	Amount             float32 `json:"amount"`
	Rate               float32 `json:"rate"`
	ConvertedValue     float32 `json:"converted_value"`
}

func (log *Log) setInfos(currency Currency, targetCurrencyCode string, creationDate int64, amount float32) {
	log.BaseCurrencyCode = currency.Base
	log.TargetCurrencyCode = targetCurrencyCode
	log.CreationDate = creationDate
	log.Amount = amount
	log.Rate = currency.getRate(log.TargetCurrencyCode)
	log.ConvertedValue = currency.getConvertedValue(log.Amount, log.Rate)
}

func (log Log) createLog() error {
	database, err := sql.Open(driverName, psqlInfo)
	CheckErr(err)
	defer database.Close()

	statement, err := database.Prepare(`INSERT INTO logs(	base_currency_code, target_currency_code, creation_date, 
                												amount, rate, converted_value)
											  VALUES ($1, $2, $3, $4, $5, $6)`)
	_, err = statement.Exec(log.BaseCurrencyCode, log.TargetCurrencyCode, log.CreationDate, log.Amount, log.Rate,
		log.ConvertedValue)
	CheckErr(err)

	return err
}

func getLastInsertedLog() Log {
	database, err := sql.Open(driverName, psqlInfo)
	CheckErr(err)
	defer database.Close()

	var log Log

	result, err := database.Query(`SELECT id, base_currency_code, target_currency_code, creation_date, 
                						 amount, rate, converted_value FROM logs ORDER BY creation_date DESC LIMIT 1`)
	CheckErr(err)

	for result.Next() {
		err = result.Scan(&log.Id, &log.BaseCurrencyCode, &log.TargetCurrencyCode, &log.CreationDate, &log.Amount,
			&log.Rate, &log.ConvertedValue)
		CheckErr(err)
	}

	return log
}

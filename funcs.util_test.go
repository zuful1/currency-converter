package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"os"
	"strconv"
	"testing"
)

func TestStringToFloat32(t *testing.T) {
	var stringDecimal string = "13.37"

	if _, err := strconv.ParseFloat(stringDecimal, 32); err != nil {
		t.Error("An error occured while trying to convert string to float32", err)
	}

}

func TestStringToInt64(t *testing.T) {
	var stringNumber string = "12345678910"

	_, err := strconv.ParseInt(stringNumber, 10, 64)

	if err != nil {
		t.Error("An error occured while trying to convert string to int64", err)
	}
}

func init() {
	err := godotenv.Load("db.env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	driverName = "postgres"
	host = os.Getenv("CC_HOST")
	port = os.Getenv("CC_PORT")
	user = os.Getenv("CC_USER")
	password = os.Getenv("CC_PASSWORD")
	dbname = os.Getenv("CC_DB_NAME")

	psqlInfo = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s",
		host, port, user, password, dbname)
}

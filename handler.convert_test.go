package main

import (
	"strconv"
	"testing"
	"time"
)

func TestHandlerCurrencyConvert(t *testing.T) {
	// initialize the variables for the handler
	var amount float32 = stringToFloat32("13.37")
	var date int64 = stringToInt64(strconv.FormatInt(time.Now().Unix(), 10))
	var baseCurrencyCode string = "USD"
	var targetCurrencyCode string = "EUR"

	// initialize the currency type
	var currency Currency
	var _ error = currency.initCurrency(baseCurrencyCode)

	var rate float32 = currency.getRate(targetCurrencyCode)
	var convertedValue float32 = currency.getConvertedValue(amount, rate)
	var result = struct {
		ConvertedValue float32 `json:"converted_value"`
	}{
		ConvertedValue: convertedValue,
	}

	var log Log
	log.setInfos(currency, targetCurrencyCode, date, amount)

	_ = log.createLog()

	if result.ConvertedValue == 0 {
		t.Error("Failed to get the converted value.")
	}
}

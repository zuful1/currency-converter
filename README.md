# Currency Converter

Currency Converter is an API that allows users to convert an amount from one currency to another.
The user launches a GET query to the convert-currency endpoint and retrieves a json containing the results.
A total of 170 currencies are available for conversion.

## Setup
- [ ] Put the .env file that's been given to you by email or otherwise at the root of the repository.
- [ ] Open the terminal, navigate to the root of the repository and download the dependency modules
```
go mod download
```
```
go mod tidy
```
- [ ] Build the executable through the following command:
- on Linux or Mac
```
go build -o currency-converter 
```
- on Windows
```
go build -o currency-converter.exe 
```

## Launch the server
- [ ] Execute the binary formerly built with the following command:
- on Linux and Mac
```
./currency-converter
```
- on Windows
```
currency-converter.exe
```

## Convert endpoint call parameters
```
/convert-currency/:date/:amount/:base/:target
```
- [ ] date: a unix timestamp of the time the query is made
- [ ] amount: the amount to be converted
- [ ] base: the currency code of the currency the conversion is made from (ex: 'EUR' for euros)
- [ ] target: the currency code of the currency the conversion is made to (ex: 'USD' for dollars)

## Testing
- [ ] In order to launch the unit and integration tests, enter the following command in the terminal:

```
go test
```

## Data querying from Hasura
- [ ] To fetch the data from the Hasura Graphql API make à POST request to the following endpoint :

```
https://promoted-skunk-90.hasura.app/v1/graphql
```
- [ ] You'll need to add the collaborator token in headers which information have been sent to you by email or otherwise for your query to be accepted
- [ ] Here's an example of what the body of the request should look like:
```
{
    "query": "query {logs {id, amount, rate, creation_date, converted_value, base_currency_code, target_currency_code}}"
}
```
 
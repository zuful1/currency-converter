package main

import (
	"encoding/json"
	"net/http"
)

type Currency struct {
	Base  string             `json:"base"`
	Rates map[string]float32 `json:"rates"`
}

func (currency *Currency) initCurrency(baseCurrencyCode string) error {
	var endpoint string = "https://api.exchangerate.host/latest?base=" + baseCurrencyCode

	client := http.Client{}
	request, err := http.NewRequest("GET", endpoint, nil)
	CheckErr(err)

	resp, err := client.Do(request)
	CheckErr(err)

	err = json.NewDecoder(resp.Body).Decode(currency)
	CheckErr(err)

	return err
}

func (currency *Currency) getRate(targetCurrencyCode string) float32 {
	return currency.Rates[targetCurrencyCode]
}

func (currency *Currency) getConvertedValue(amount, rate float32) float32 {
	return amount * rate
}

package main

import (
	"testing"
)

func TestCurrencyInit(t *testing.T) {
	var baseCurrencyCode string = "USD"
	var currency Currency
	var err error = currency.initCurrency(baseCurrencyCode)

	if err != nil {
		t.Error("The initialization of currency failed.", err)
	}
}

func TestGetRate(t *testing.T) {
	var baseCurrencyCode string = "USD"
	var targetCurrencyCode string = "EUR"
	var currency Currency
	currency.initCurrency(baseCurrencyCode)
	var rate float32 = currency.getRate(targetCurrencyCode)

	if rate == 0 {
		t.Error("Fail to retrieve the rate.")
	}
}

func TestGetConvertedValue(t *testing.T) {
	var baseCurrencyCode string = "USD"
	var targetCurrencyCode string = "EUR"
	var currency Currency
	currency.initCurrency(baseCurrencyCode)

	var amount float32 = 100
	var rate float32 = currency.getRate(targetCurrencyCode)
	var convertedValue float32 = currency.getConvertedValue(amount, rate)

	if convertedValue == 0 {
		t.Error("Failed to convert value")
	}
}

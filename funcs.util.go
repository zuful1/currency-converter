package main

import (
	"log"
	"strconv"
)

func CheckErr(err error) {
	if err != nil {
		log.Println(err)
	}
}

func stringToFloat32(str string) float32 {
	var output float64

	if s, err := strconv.ParseFloat(str, 32); err == nil {
		output = s
		CheckErr(err)
	}

	return float32(output)
}

func stringToInt64(str string) int64 {
	output, err := strconv.ParseInt(str, 10, 64)
	CheckErr(err)

	return output
}

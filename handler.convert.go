package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func handlerCurrencyConvert(c *gin.Context) {
	// initialize the variables for the handler
	var code int = http.StatusOK
	var amount float32 = stringToFloat32(c.Param("amount"))
	var date int64 = stringToInt64(c.Param("date"))
	var baseCurrencyCode string = c.Param("base")
	var targetCurrencyCode string = c.Param("target")

	// initialize the currency type
	var currency Currency
	var err error = currency.initCurrency(baseCurrencyCode)
	if err != nil {
		code = http.StatusInternalServerError
	}

	var rate float32 = currency.getRate(targetCurrencyCode)
	var convertedValue float32 = currency.getConvertedValue(amount, rate)
	var result = struct {
		ConvertedValue float32 `json:"converted_value"`
	}{
		ConvertedValue: convertedValue,
	}

	var log Log
	log.setInfos(currency, targetCurrencyCode, date, amount)

	err = log.createLog()
	if err != nil {
		code = http.StatusInternalServerError
	}

	c.JSON(code, result)
}

package main

import (
	"testing"
	"time"
)

func TestSetInfos(t *testing.T) {
	var baseCurrency string = "USD"
	var targetCurrency string = "EUR"
	var amount float32 = 13.37
	var currentTime int64 = time.Now().Unix()
	var currency Currency
	var err error = currency.initCurrency(baseCurrency)
	CheckErr(err)
	var log Log
	log.setInfos(currency, targetCurrency, currentTime, amount)

	if log.BaseCurrencyCode == "" {
		t.Error("Failed to set the log infos")
	}
}

func TestGetLastInsertedLog(t *testing.T) {
	var lastInsertedLog Log = getLastInsertedLog()

	if lastInsertedLog.Id == 0 {
		t.Error("The last inserted log doesn't match.")
	}
}

func TestCreateLog(t *testing.T) {
	var baseCurrency string = "USD"
	var targetCurrency string = "EUR"
	var amount float32 = 13.37
	var currentTime int64 = time.Now().Unix()
	var currency Currency
	var err error = currency.initCurrency(baseCurrency)

	var log Log
	log.setInfos(currency, targetCurrency, currentTime, amount)

	err = log.createLog()
	if err != nil {
		t.Error("The insertion of the record failed.", err)
	}

	var lastInsertedLog Log = getLastInsertedLog()

	if lastInsertedLog.CreationDate != currentTime ||
		lastInsertedLog.Amount != amount ||
		lastInsertedLog.Rate != currency.getRate(targetCurrency) {

		t.Error("The last inserted log doesn't match.")
	}
}
